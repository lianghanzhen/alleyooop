package com.viewpagerindicator;

import android.view.View;

public interface TabPageAdapter {

    View getTabView(int position);

}
