package com.lianghanzhen.alleyooop.models;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.lianghanzhen.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class Shot implements Parcelable {

    public Shot() {}

    @SerializedName("id")
    public int mId;

    @SerializedName("title")
    public String mTitle;

    @SerializedName("url")
    public String mUrl;

    @SerializedName("short_url")
    public String mShortUrl;

    @SerializedName("image_url")
    public String mImageUrl;

    @SerializedName("image_teaser_url")
    public String mImageTeaserUrl;

    @SerializedName("image_400_url")
    public String mImage400Url;

    @SerializedName("width")
    public int mWidth;

    @SerializedName("height")
    public int mHeight;

    @SerializedName("views_count")
    public int mViewsCount;

    @SerializedName("likes_count")
    public int mLikesCount;

    @SerializedName("comments_count")
    public int mCommentsCount;

    @SerializedName("rebounds_count")
    public int mReboundsCount;

    @SerializedName("rebound_source_id")
    public int mReboundSourceId;

    @SerializedName("created_at")
    public Date mCreatedAt;

    @SerializedName("player")
    public Player mPlayer;

    //region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeString(mUrl);
        dest.writeString(mShortUrl);
        dest.writeString(mImageUrl);
        dest.writeString(mImageTeaserUrl);
        dest.writeString(mImage400Url);
        dest.writeInt(mWidth);
        dest.writeInt(mHeight);
        dest.writeInt(mViewsCount);
        dest.writeInt(mLikesCount);
        dest.writeInt(mCommentsCount);
        dest.writeInt(mReboundsCount);
        dest.writeInt(mReboundSourceId);
        dest.writeLong(mCreatedAt != null ? mCreatedAt.getTime() : 0);
        dest.writeParcelable(mPlayer, flags);
    }

    public Shot(Parcel source) {
        mId = source.readInt();
        mTitle = source.readString();
        mUrl = source.readString();
        mShortUrl = source.readString();
        mImageUrl = source.readString();
        mImageTeaserUrl = source.readString();
        mImage400Url = source.readString();
        mWidth = source.readInt();
        mHeight = source.readInt();
        mViewsCount = source.readInt();
        mLikesCount = source.readInt();
        mCommentsCount = source.readInt();
        mReboundsCount = source.readInt();
        mReboundSourceId = source.readInt();
        long createdAt = source.readLong();
        mCreatedAt = createdAt == 0 ? new Date() : new Date(createdAt);
        mPlayer = source.readParcelable(getClass().getClassLoader());
    }

    public static final Creator<Shot> CREATOR = new Creator<Shot>() {

        @Override
        public Shot createFromParcel(Parcel source) {
            return new Shot(source);
        }

        @Override
        public Shot[] newArray(int size) {
            return new Shot[size];
        }

    };

    //endregion

    public static class Shots extends PagerResult<Shot> {

        @SerializedName("shots")
        public List<Shot> mShots;

        @Override
        public List<Shot> getResults() {
            final List<Shot> results = new ArrayList<Shot>();
            if (mShots != null && !mShots.isEmpty()) {
                results.addAll(mShots);
            }
            return results;
        }

    }

}
