package com.lianghanzhen.alleyooop.models;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.lianghanzhen.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class Player implements Parcelable {

    public Player() {}

    @SerializedName("id")
    public int mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("username")
    public String mUsername;

    @SerializedName("url")
    public String mUrl;

    @SerializedName("website_url")
    public String mWebsiteUrl;

    @SerializedName("avatar_url")
    public String mAvatarUrl;

    @SerializedName("location")
    public String mLocation;

    @SerializedName("twitter_screen_name")
    public String mTwitterName;

    @SerializedName("drafted_by_player_id")
    public int mDraftedByPlayerId;

    @SerializedName("shots_count")
    public int mShotsCount;

    @SerializedName("draftees_count")
    public int mDrafteesCount;

    @SerializedName("followers_count")
    public int mFollowersCount;

    @SerializedName("following_count")
    public int mFollowingCount;

    @SerializedName("comments_count")
    public int mCommentsCount;

    @SerializedName("comments_received_count")
    public int mCommentReceivedCount;

    @SerializedName("likes_count")
    public int mLikesCount;

    @SerializedName("likes_received_count")
    public int mLikesReceivedCount;

    @SerializedName("rebounds_count")
    public int mReboundsCount;

    @SerializedName("rebounds_received_count")
    public int mReboundsReceivedCount;

    @SerializedName("created_at")
    public Date mCreatedAt;

    //region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeString(mUsername);
        dest.writeString(mUrl);
        dest.writeString(mWebsiteUrl);
        dest.writeString(mAvatarUrl);
        dest.writeString(mLocation);
        dest.writeString(mTwitterName);
        dest.writeInt(mDraftedByPlayerId);
        dest.writeInt(mShotsCount);
        dest.writeInt(mDrafteesCount);
        dest.writeInt(mFollowersCount);
        dest.writeInt(mFollowingCount);
        dest.writeInt(mCommentsCount);
        dest.writeInt(mCommentReceivedCount);
        dest.writeInt(mLikesCount);
        dest.writeInt(mLikesReceivedCount);
        dest.writeInt(mReboundsCount);
        dest.writeInt(mReboundsReceivedCount);
        dest.writeLong(mCreatedAt != null ? mCreatedAt.getTime() : 0);
    }

    public Player(Parcel source) {
        mId = source.readInt();
        mName = source.readString();
        mUsername = source.readString();
        mUrl = source.readString();
        mWebsiteUrl = source.readString();
        mAvatarUrl = source.readString();
        mLocation = source.readString();
        mTwitterName = source.readString();
        mDraftedByPlayerId = source.readInt();
        mShotsCount = source.readInt();
        mDrafteesCount = source.readInt();
        mFollowersCount = source.readInt();
        mFollowingCount = source.readInt();
        mCommentsCount = source.readInt();
        mCommentReceivedCount = source.readInt();
        mLikesCount = source.readInt();
        mLikesReceivedCount = source.readInt();
        mReboundsCount = source.readInt();
        mReboundsReceivedCount = source.readInt();
        long created = source.readLong();
        mCreatedAt = created == 0 ? new Date() : new Date(created);
    }

    public static final Creator<Player> CREATOR = new Creator<Player>() {

        @Override
        public Player createFromParcel(Parcel source) {
            return new Player(source);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }

    };

    //endregion

    public static class Players extends PagerResult<Player> {

        @SerializedName("players")
        public List<Player> mPlayers;

        @Override
        public List<Player> getResults() {
            final List<Player> results = new ArrayList<Player>();
            if (mPlayers != null && !mPlayers.isEmpty()) {
                results.addAll(mPlayers);
            }
            return results;
        }

    }

}
