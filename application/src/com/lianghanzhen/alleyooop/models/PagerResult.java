package com.lianghanzhen.alleyooop.models;

import com.google.gson.lianghanzhen.annotations.SerializedName;

import java.util.List;

public abstract class PagerResult<T> {

    @SerializedName("page")
    public int mCurrentPage;

    @SerializedName("pages")
    public int mTotalPages;

    @SerializedName("per_page")
    public int mCountOfPerPage;

    @SerializedName("total")
    public int mTotalCount;

    @SerializedName("message")
    public String mMessage;

    public abstract List<T> getResults();

}
