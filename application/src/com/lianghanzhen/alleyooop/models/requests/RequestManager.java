package com.lianghanzhen.alleyooop.models.requests;


import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;

public final class RequestManager {

    private RequestManager() {}

    private static RequestQueue sRequestQueue = newRequestQueue();

    private static RequestQueue newRequestQueue() {
        RequestQueue requestQueue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        requestQueue.start();
        return requestQueue;
    }

    public static <T> void addRequest(GsonRequest<T> request, Object tag) {
        if (tag != null) {
            request.setTag(tag);
        }
        sRequestQueue.add(request);
    }

    public static void cancelRequests(Object tag) {
        sRequestQueue.cancelAll(tag);
    }

}
