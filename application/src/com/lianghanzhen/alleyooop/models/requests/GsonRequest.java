package com.lianghanzhen.alleyooop.models.requests;

import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.lianghanzhen.JsonSyntaxException;
import com.lianghanzhen.alleyooop.utils.GsonUtil;

import java.io.UnsupportedEncodingException;

public class GsonRequest<T> extends Request<T> {

    private final Class<T> mClazz;
    private final Response.Listener<T> mListener;

    public GsonRequest(String url, Class<T> clazz, Response.ErrorListener errorListener, Response.Listener<T> listener) {
        super(Method.GET, url, errorListener);
        mClazz = clazz;
        mListener = listener;
        Log.d("Volley", String.format("Send: %s", url));
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        try {
            final String json = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
            Log.d("Volley", String.format("Receive: %s", json));
            return Response.success(GsonUtil.getGson().fromJson(json, mClazz), HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

}
