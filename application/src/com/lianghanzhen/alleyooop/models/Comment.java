package com.lianghanzhen.alleyooop.models;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.lianghanzhen.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class Comment implements Parcelable {

    public Comment() {}

    @SerializedName("id")
    public int mId;

    @SerializedName("body")
    public String mBody;

    @SerializedName("likes_count")
    public int mLikesCount;

    @SerializedName("created_at")
    public Date mCreatedAt;

    @SerializedName("player")
    public Player mPlayer;

    //region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mBody);
        dest.writeInt(mLikesCount);
        dest.writeLong(mCreatedAt.getTime());
        dest.writeParcelable(mPlayer, flags);
    }

    public Comment(Parcel source) {
        mId = source.readInt();
        mBody = source.readString();
        mLikesCount = source.readInt();
        long createdAt = source.readLong();
        mCreatedAt = createdAt != 0 ? new Date(createdAt) : new Date();
        mPlayer = source.readParcelable(getClass().getClassLoader());
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {

        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }

    };

    //endregion

    public static class Comments extends PagerResult<Comment> {

        @SerializedName("comments")
        public List<Comment> mComments;

        @Override
        public List<Comment> getResults() {
            final List<Comment> results = new ArrayList<Comment>();
            if (mComments != null && !mComments.isEmpty()) {
                results.addAll(mComments);
            }
            return results;
        }

    }

}
