package com.lianghanzhen.alleyooop;


import android.content.Context;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;

public final class Config {

    private Config() {}

    public static final String sDribbbleApiUrl = "http://api.dribbble.com";

    public static final SimpleDateFormat sDateFormat = new SimpleDateFormat("yy/MM/dd");

    public static final String sAppName = "Alleyooop";

    public static final int INDEX_AVATAR_PLACEHOLDER = 0;
    public static final int INDEX_SHOT_PLACEHOLDER = 1;

    public static final class UmengEvents {

        private UmengEvents() {}

        public static final String SHOTS_DETAIL = "Shots_Detail";
        public static final String SHOTS_COMMENTS = "Shots_Comments";
        public static final String SHOTS_REBOUNDS = "Shots_Rebounds";
        public static final String SHOTS_POPULAR = "Shots_Popular";
        public static final String SHOTS_EVERYONE = "Shots_Everyone";
        public static final String SHOTS_DEBUTS = "Shots_Debuts";
        public static final String SHOTS_PLAYER_FOLLOWING = "Shots_Player_Following";
        public static final String SHOTS_PLAYER_LIKES = "Shots_Player_Likes";
        public static final String SHOTS_PLAYER_SHOTS = "Shots_Player_Shots";

        public static final String PLAYERS_DETAIL = "Players_Detail";
        public static final String PLAYERS_FOLLOWING = "Players_Following";
        public static final String PLAYERS_FOLLOWERS = "Players_Followers";
        public static final String PLAYERS_DRAFTEES = "Players_Draftees";

        public static final String MINE = "Mine";

    }

    public static final class UmengOnlineParams {

        private UmengOnlineParams() {}

        public static boolean isPushAdEnabled(Context context) {
            return getBooleanValue(context, "PushAdEnabled");
        }

        public static boolean isSpotAdEnabled(Context context) {
            return getBooleanValue(context, "SpotAdEnabled");
        }

        public static boolean isShotDetailPushAdEnabled(Context context) {
            return getBooleanValue(context, "ShotDetailPushAdEnabled");
        }

        public static boolean isShotDetailSpotAdEnabled(Context context) {
            return getBooleanValue(context, "ShotDetailSpotAdEnabled");
        }

        public static boolean isPlayerDetailPushAdEnabled(Context context) {
            return getBooleanValue(context, "PlayerDetailPushAdEnabled");
        }

        public static boolean isPlayerDetailSpotAdEnabled(Context context) {
            return getBooleanValue(context, "PlayerDetailSpotAdEnabled");
        }

        public static boolean isShotListPushAdEnabled(Context context) {
            return getBooleanValue(context, "ShotListPushAdEnabled");
        }

        public static boolean isShotListSpotAdEnabled(Context context) {
            return getBooleanValue(context, "ShotListSpotAdEnabled");
        }

        public static boolean isShowSpotAdOnlyInWifi(Context context) {
            return getBooleanValue(context, "ShowSpotAdOnlyInWifi");
        }

        public static int getShotListAdPage(Context context, int defaultValue) {
            return getIntValue(context, "ShotListAdPage", defaultValue);
        }

        public static int getPushMode(Context context, int defaultValue) {
            return getIntValue(context, "PushMode", defaultValue);
        }

        public static int getSpotAdFreeTimes(Context context, int defaultValue) {
            return getIntValue(context, "SpotAdFreeTimes", defaultValue);
        }

        private static boolean getBooleanValue(Context context, String key) {
            return "on".equalsIgnoreCase(MobclickAgent.getConfigParams(context, key));
        }

        private static int getIntValue(Context context, String key, int defaultValue) {
            int result = defaultValue;
            try {
                result = Integer.parseInt(MobclickAgent.getConfigParams(context, key));
            } catch (Exception e) {
                // ignore
            } finally {
                return result;
            }
        }

    }

}
