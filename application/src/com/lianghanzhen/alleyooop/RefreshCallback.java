package com.lianghanzhen.alleyooop;


public interface RefreshCallback {

    void onRefresh();

}
