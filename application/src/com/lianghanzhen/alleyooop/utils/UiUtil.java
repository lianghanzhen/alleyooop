package com.lianghanzhen.alleyooop.utils;


import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.widget.ListView;
import android.widget.TextView;

import java.util.regex.Pattern;

public final class UiUtil {

    public static final int ANIMATION_FADE_IN_TIME = 250;
    private static final Pattern REGEX_HTML_ESCAPE = Pattern.compile(".*&\\S;.*");

    private UiUtil() {}

    public static void smoothScrollListView(ListView listView, int position) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            listView.smoothScrollToPosition(position);
        } else {
            listView.setSelection(position);
        }
    }

    public static void smoothScrollListViewToTop(final ListView listView) {
        if (listView != null) {
            smoothScrollListView(listView, 0);
            listView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(0);
                }
            }, 300);
        }
    }

    public static boolean isOrientationPortrait(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static boolean isScreenSmall(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) < Configuration.SCREENLAYOUT_SIZE_NORMAL;
    }

    public static int getColumnCount(Context context) {
        boolean isTablet = isTablet(context);
        boolean isOrientationPortrait = isOrientationPortrait(context);
        return isScreenSmall(context) ? 1 : isTablet && isOrientationPortrait ? 2 : isTablet ? 3 : isOrientationPortrait ? 1 : 2;
    }

    public static int getPlayerColumnCount(Context context) {
        return getColumnCount(context) + 1;
    }

    public static int getRotationAngle(Context context) {
        return isTablet(context) && !isOrientationPortrait(context) ? 1080 : 360;
    }

    public static int getRotationDuration(Context context) {
        return isTablet(context) && !isOrientationPortrait(context) ? 1000 : 600;
    }

    public static int getCountOfPerPage(Context context) {
        boolean isTablet = isTablet(context);
        boolean isOrientationPortrait = isOrientationPortrait(context);
        return isScreenSmall(context) ? 8 : isTablet && isOrientationPortrait ? 12 : isTablet ? 15 : 12;
    }

    public static void setTextMaybeHtml(TextView view, String text) {
        if (TextUtils.isEmpty(text)) {
            view.setText("");
            return;
        }
        if ((text.contains("<") && text.contains(">")) || REGEX_HTML_ESCAPE.matcher(text).find()) {
            view.setText(Html.fromHtml(text));
            view.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            view.setText(text);
        }
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    public static boolean hasJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

}
