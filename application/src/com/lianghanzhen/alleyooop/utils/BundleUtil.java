package com.lianghanzhen.alleyooop.utils;


import android.os.Bundle;
import android.os.Parcelable;

import java.io.Serializable;

public final class BundleUtil {

    private BundleUtil() {}

    public static String getString(Bundle bundle, String key, String defaultValue) {
        return bundle != null && bundle.containsKey(key) ? bundle.getString(key) : defaultValue;
    }

    public static boolean getBoolean(Bundle bundle, String key, boolean defaultValue) {
        return bundle != null && bundle.containsKey(key) ? bundle.getBoolean(key) : defaultValue;
    }

    public static int getInt(Bundle bundle, String key, int defaultValue) {
        return bundle != null && bundle.containsKey(key) ? bundle.getInt(key) : defaultValue;
    }

    public static <T extends Serializable> T getSerializable(Bundle bundle, String key, T defaultValue) {
        return bundle != null && bundle.containsKey(key) ? (T) bundle.getSerializable(key) : defaultValue;
    }

    public static <T extends Parcelable> T getParcelable(Bundle bundle, String key, T defaultValue) {
        return bundle != null && bundle.containsKey(key) ? (T) bundle.getParcelable(key) : defaultValue;
    }

}
