package com.lianghanzhen.alleyooop.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.lianghanzhen.alleyooop.Config;

public final class NetUtil {

    private static final String TAG = "NetUtil";
    private static String mUserAgent = null;

    private NetUtil() {}

    public static String getUserAgent(Context mContext) {
        if (mUserAgent == null) {
            mUserAgent = Config.sAppName;
            try {
                String packageName = mContext.getPackageName();
                String version = mContext.getPackageManager().getPackageInfo(packageName, 0).versionName;
                mUserAgent = mUserAgent + " (" + packageName + "/" + version + ")";
                Log.d(TAG, "User agent set to: " + mUserAgent);
            } catch (PackageManager.NameNotFoundException e) {
                Log.d(TAG, "Unable to find self by package name", e);
            }
        }
        return mUserAgent;
    }

}
