package com.lianghanzhen.alleyooop.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import com.lianghanzhen.alleyooop.models.Player;

public final class PlayerUtil {

    private static final String PREFERENCES_PLAYER = "Player";
    private static final String KEY_PLAYER_NAME = "PlayerUtil.player.name";
    private static final String KEY_PLAYER = "PlayerUtil.player";

    private PlayerUtil() {}

    public static String getPlayerName(Context context) {
        return getString(context, KEY_PLAYER_NAME);
    }

    public static void savePlayerName(Context context, String playerName) {
        setString(context, KEY_PLAYER_NAME, playerName);
    }

    public static Player getPlayer(Context context) {
        String json = getString(context, KEY_PLAYER);
        return TextUtils.isEmpty(json) ? null : GsonUtil.getGson().fromJson(json, Player.class);
    }

    public static void savePlayer(Context context, Player player) {
        if (player != null) {
            setString(context, KEY_PLAYER, GsonUtil.getGson().toJson(player));
        }
    }

    private static String getString(Context context, String key) {
        return context.getSharedPreferences(PREFERENCES_PLAYER, Context.MODE_PRIVATE).getString(key, null);
    }

    private static void setString(Context context, String key, String value) {
        if (!TextUtils.isEmpty(value)) {
            final SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_PLAYER, Context.MODE_PRIVATE).edit().putString(key, value);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
                editor.apply();
            } else {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        editor.commit();
                        return null;
                    }
                }.execute();
            }
        }
    }

    public static void clear(Context context) {
        context.getSharedPreferences(PREFERENCES_PLAYER, Context.MODE_PRIVATE).edit().clear().commit();
    }

}
