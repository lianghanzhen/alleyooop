package com.lianghanzhen.alleyooop.utils;


import com.google.gson.lianghanzhen.Gson;
import com.google.gson.lianghanzhen.GsonBuilder;
import com.google.gson.lianghanzhen.JsonArray;
import com.google.gson.lianghanzhen.JsonObject;
import com.lianghanzhen.alleyooop.models.deserializers.DateTypeDeserializer;

import java.util.Date;

public final class GsonUtil {

    private GsonUtil() {}

    private static Gson sGson;

    public static Gson getGson() {
        if (sGson == null) {
            sGson = new GsonBuilder().registerTypeAdapter(Date.class, new DateTypeDeserializer("yyyy/MM/dd HH:mm:ss -ssms")).create();
        }
        return sGson;
    }

    public static int getInt(JsonObject jsonObject, String key, int defaultValue) {
        return jsonObject.has(key) ? jsonObject.get(key).getAsInt() : defaultValue;
    }

    public static int getInt(JsonObject jsonObject, String key) {
        return getInt(jsonObject, key, 0);
    }

    public static String getString(JsonObject jsonObject, String key, String defaultValue) {
        return jsonObject.has(key) ? jsonObject.get(key).getAsString() : defaultValue;
    }

    public static String getString(JsonObject jsonObject, String key) {
        return getString(jsonObject, key, "");
    }

    public static JsonArray getJsonArray(JsonObject jsonObject, String key, JsonArray defaultValue) {
        return jsonObject.has(key) ? jsonObject.get(key).getAsJsonArray() : defaultValue;
    }

    public static JsonArray getJsonArray(JsonObject jsonObject, String key) {
        return getJsonArray(jsonObject, key, new JsonArray());
    }

}
