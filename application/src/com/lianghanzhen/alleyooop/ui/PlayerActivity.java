package com.lianghanzhen.alleyooop.ui;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Player;
import com.lianghanzhen.alleyooop.models.requests.GsonRequest;
import com.lianghanzhen.alleyooop.models.requests.RequestManager;
import com.lianghanzhen.alleyooop.ui.adapters.LazyableFragmentStatePagerAdapter;
import com.lianghanzhen.alleyooop.ui.fragments.PlayerFragment;
import com.lianghanzhen.alleyooop.ui.fragments.ShotFragment;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.lianghanzhen.alleyooop.utils.PlayerUtil;
import com.umeng.analytics.MobclickAgent;
import com.viewpagerindicator.PageIndicator;

public class PlayerActivity extends BaseActionBarActivity {

    private static final String KEY_PLAYER = "KEY_PLAYER";
    private static final String KEY_PLAYER_NAME = "KEY_PLAYER_NAME";
    private static final String KEY_PLAYER_TYPE = "KEY_PLAYER_TYPE";
    private static final String KEY_CURRENT_ITEM = "KEY_CURRENT_ITEM";

    public static void startActivity(FragmentActivity activity, Player player) {
        Intent intent = new Intent(activity, PlayerActivity.class);
        intent.putExtra(KEY_PLAYER, player);
        activity.startActivity(intent);
    }

    public static void startActivity(FragmentActivity activity, String playerName) {
        Intent intent = new Intent(activity, PlayerActivity.class);
        intent.putExtra(KEY_PLAYER_NAME, playerName);
        activity.startActivity(intent);
    }

    private final View.OnClickListener sOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            refreshViewPager(mPlayerType == PlayerType.SHOTS ? PlayerType.FOLLOWING : PlayerType.SHOTS, (Integer) view.getTag(R.id.tag_player_index));
        }
    };

    private Player mPlayer;
    private String mPlayerName;

    private ViewPager mPager;
    private PageIndicator mIndicator;
    private ImageView mAvatarView;
    private TextView mNameView;
    private TextView mFollowingView;
    private TextView mFollowersView;
    private TextView mDrafteesView;

    private PlayerType mPlayerType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPager = (ViewPager) findViewById(R.id.activity_player_pager);
        mIndicator = (PageIndicator) findViewById(R.id.activity_player_indicator);
        mAvatarView = (ImageView) findViewById(R.id.activity_player_avatar);
        mNameView = (TextView) findViewById(R.id.activity_player_name);
        mFollowingView = (TextView) findViewById(R.id.activity_player_following);
        mFollowersView = (TextView) findViewById(R.id.activity_player_followers);
        mDrafteesView = (TextView) findViewById(R.id.activity_player_draftees);
        configureClickListener(R.id.activity_player_following, R.id.activity_player_followers, R.id.activity_player_draftees);

        mPlayer = BundleUtil.getParcelable(savedInstanceState == null ? getIntent().getExtras() : savedInstanceState, KEY_PLAYER, null);
        mPlayerType = BundleUtil.getSerializable(savedInstanceState, KEY_PLAYER_TYPE, PlayerType.SHOTS);
        if (mPlayer == null) {
            mPlayerName = BundleUtil.getString(savedInstanceState == null ? getIntent().getExtras() : savedInstanceState, KEY_PLAYER_NAME, null);
        } else {
            refreshViews();
            mPlayerName = mPlayer.mUsername;
        }
        sendPlayerRequest(mPlayerName, mPlayer == null);
        refreshViewPager(mPlayerType, BundleUtil.getInt(savedInstanceState, KEY_CURRENT_ITEM, 0));
    }

    private void configureClickListener(int ... ids) {
        for (int i = 0, length = ids.length; i < length; i++) {
            View view = findViewById(ids[i]);
            view.setTag(R.id.tag_player_index, i);
            view.setOnClickListener(sOnClickListener);
        }
    }

    private void sendPlayerRequest(String playerName, final boolean savePlayer) {
        RequestManager.addRequest(new GsonRequest<Player>(String.format("%s/%s", Config.sDribbbleApiUrl, playerName), Player.class, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    refreshViews();
                }
            }, new Response.Listener<Player>() {
                @Override
                public void onResponse(Player player) {
                    mPlayer = player;
                    refreshViews();
                    if (savePlayer) {
                        PlayerUtil.savePlayer(PlayerActivity.this, mPlayer);
                    }
                }
            }), this);
    }

    private void refreshViews() {
        refreshActionBar();
        if (mPlayer != null) {
            loadAvatarImage(mPlayer.mAvatarUrl, mAvatarView);
            mNameView.setText(mPlayer.mName);
        } else {
            mNameView.setText("Unknown");
        }
        refreshPlayerInfoViews();
    }

    private void refreshActionBar() {
        if (mPlayer != null) {
            loadAvatarImage(mPlayer.mAvatarUrl, mIconView);
            mTitleView.setText(mPlayer.mName);
        } else {
            mTitleView.setText("Unknown");
        }
    }

    private void refreshPlayerInfoViews() {
        boolean isShotsType = mPlayerType == PlayerType.SHOTS;
        setPlayerTextItemView(mFollowingView, mPlayer != null ? String.valueOf(isShotsType ? mPlayer.mFollowingCount : mPlayer.mShotsCount) : "---", isShotsType ? "following" : "shots");
        setPlayerTextItemView(mFollowersView, isShotsType && mPlayer != null ? String.valueOf(mPlayer.mFollowersCount) : "---", isShotsType ? "followers" : "following");
        setPlayerTextItemView(mDrafteesView, mPlayer != null ? String.valueOf(isShotsType ? mPlayer.mDrafteesCount : mPlayer.mLikesCount) : "---", isShotsType ? "draftees" : "likes");
    }

    private void setPlayerTextItemView(TextView itemView, String count, String title) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int start = builder.length();
        builder.append(count).append("\n");
        builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.pink)), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new StyleSpan(Typeface.BOLD), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new RelativeSizeSpan(1.2f), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        start = builder.length();
        builder.append(title);
        builder.setSpan(new StyleSpan(Typeface.ITALIC), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        itemView.setText(builder);
    }

    private void refreshViewPager(PlayerType type, int index) {
        mPlayerType = type;
        PagerAdapter adapter = type == PlayerType.SHOTS ? new PlayerShotsPagerAdapter(getSupportFragmentManager(), mPlayerName)
                                                        : new PlayerFollowingPagerAdapter(getSupportFragmentManager(), mPlayerName);
        if (mPager.getAdapter() == null) {
            mPager.setAdapter(adapter);
            mIndicator.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    MobclickAgent.onEvent(PlayerActivity.this, mPager.getAdapter() instanceof PlayerShotsPagerAdapter ? PlayerShotsPagerAdapter.PlayerShotsType.getUmengEvent(position)
                                                                                                 : PlayerFollowingPagerAdapter.PlayerFollowingType.getUmengEvent(position));
                }
            });
            mIndicator.setViewPager(mPager, index);
        } else {
            mPager.setAdapter(adapter);
            mPager.setCurrentItem(index);
            mIndicator.notifyDataSetChanged();
        }
        refreshPlayerInfoViews();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_player;
    }

    @Override
    protected int getCustomActionBarViewResId() {
        return R.layout.action_bar_home_layout_with_player;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_PLAYER, mPlayer);
        outState.putString(KEY_PLAYER_NAME, mPlayerName);
        outState.putSerializable(KEY_PLAYER_TYPE, mPlayerType);
        outState.putInt(KEY_CURRENT_ITEM, mPager.getCurrentItem());
    }

    @Override
    protected String getScreenName() {
        return "PlayerActivity";
    }

    //region PlayerType

    static enum PlayerType {
        SHOTS, FOLLOWING
    }

    //endregion

    //region PlayerShotsPagerAdapter

    static class PlayerShotsPagerAdapter extends LazyableFragmentStatePagerAdapter {

        private final String mPlayerName;

        PlayerShotsPagerAdapter(FragmentManager fm, String playerName) {
            super(fm);
            mPlayerName = playerName;
        }

        @Override
        public Fragment getItem(int position) {
            PlayerShotsType type = PlayerShotsType.values()[position];
            return ShotFragment.newInstance(String.format(type == PlayerShotsType.SHOTS ? "/players/%s/%s" : "/players/%s/shots/%s", mPlayerName, type.getName().toLowerCase()), type != PlayerShotsType.SHOTS);
        }

        @Override
        public int getCount() {
            return PlayerShotsType.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PlayerShotsType.values()[position].getName();
        }

        //region PlayerShotsType

        static enum PlayerShotsType {

            SHOTS("Shots"), FOLLOWING("Following"), LIKES("Likes");

            private final String mName;

            private PlayerShotsType(String name) {
                mName = name;
            }

            public String getName() {
                return mName;
            }

            static String getUmengEvent(int position) {
                switch (PlayerShotsType.values()[position]) {
                    case SHOTS:
                        return Config.UmengEvents.SHOTS_PLAYER_SHOTS;
                    case FOLLOWING:
                        return Config.UmengEvents.SHOTS_PLAYER_FOLLOWING;
                    default:
                        return Config.UmengEvents.SHOTS_PLAYER_LIKES;
                }
            }

        }

        //endregion

    }

    //endregion

    //region PlayerFollowingPagerAdapter

    static final class PlayerFollowingPagerAdapter extends LazyableFragmentStatePagerAdapter {

        private final String mPlayerName;

        PlayerFollowingPagerAdapter(FragmentManager fm, String playerName) {
            super(fm);
            mPlayerName = playerName;
        }

        @Override
        public Fragment getItem(int position) {
            return PlayerFragment.newInstance(String.format("/players/%s/%s", mPlayerName,
                    PlayerFollowingType.values()[position].getName().toLowerCase()));
        }

        @Override
        public int getCount() {
            return PlayerFollowingType.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PlayerFollowingType.values()[position].getName();
        }

        //region PlayerFollowingType

        static enum PlayerFollowingType {

            FOLLOWING("Following"), FOLLOWERS("Followers"), DRAFTEES("Draftees");

            private final String mName;

            private PlayerFollowingType(String name) {
                mName = name;
            }

            public String getName() {
                return mName;
            }

            static String getUmengEvent(int position) {
                switch (PlayerFollowingType.values()[position]) {
                    case FOLLOWING:
                        return Config.UmengEvents.PLAYERS_FOLLOWING;
                    case FOLLOWERS:
                        return Config.UmengEvents.PLAYERS_FOLLOWERS;
                    default:
                        return Config.UmengEvents.PLAYERS_DRAFTEES;
                }
            }

        }

        //endregion

    }

    //endregion

}
