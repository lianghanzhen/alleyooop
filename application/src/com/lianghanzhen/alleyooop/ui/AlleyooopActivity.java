package com.lianghanzhen.alleyooop.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Player;
import com.lianghanzhen.alleyooop.ui.adapters.LazyableFragmentStatePagerAdapter;
import com.lianghanzhen.alleyooop.ui.fragments.ShotFragment;
import com.lianghanzhen.alleyooop.utils.PlayerUtil;
import com.lianghanzhen.alleyooop.utils.UiUtil;
import com.nineoldandroids.animation.*;
import com.umeng.analytics.MobclickAgent;
import com.viewpagerindicator.LinearTabPageIndicator;
import com.viewpagerindicator.SlidingLinearTabPageIndicator;

public class AlleyooopActivity extends BaseActionBarActivity implements LinearTabPageIndicator.OnTabReselectedListener {

    // region Animation direction

    private static final int DIRECTION_LEFT = 0;
    private static final int DIRECTION_RIGHT = 1;

    //endregion

    private ShotPagerAdapter mShotPagerAdapter;
    private LinearLayout mContainerView;
    private ImageView mPlayView;
    private EditText mNameView;

    private boolean mAnimatedAtStartup;

    private final View.OnClickListener sPlayClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (isDirectionLeft(view) && !TextUtils.isEmpty(mNameView.getText())) {
                PlayerUtil.savePlayerName(getApplicationContext(), mNameView.getText().toString());
                MobclickAgent.onEvent(AlleyooopActivity.this, Config.UmengEvents.MINE, "Save Player Information");
            } else {
                MobclickAgent.onEvent(AlleyooopActivity.this, Config.UmengEvents.MINE);
            }
            boolean hasPlayer = PlayerUtil.getPlayer(getApplicationContext()) != null || !TextUtils.isEmpty(PlayerUtil.getPlayerName(getApplicationContext()));
            int endX = mContainerView.getWidth() - view.getWidth();
            AnimatorSet set = new AnimatorSet();
            ValueAnimator rotation = ObjectAnimator.ofFloat(view, "rotation", 0, UiUtil.getRotationAngle(getApplicationContext()));
            if (hasPlayer && !isDirectionLeft(view)) {
                set.play(rotation);
            } else {
                set.playTogether(rotation, ObjectAnimator.ofFloat(mContainerView, "translationX", isDirectionLeft(view) ? 0 : -endX, isDirectionLeft(view) ? -endX : 0));
            }
            set.setDuration(UiUtil.getRotationDuration(getApplicationContext())).addListener(sAnimatorListener);
            set.start();
        }
    };

    private final Animator.AnimatorListener sAnimatorListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            Player player = PlayerUtil.getPlayer(getApplicationContext());
            String playerName = PlayerUtil.getPlayerName(getApplicationContext());
            boolean hasPlayer = player != null || !TextUtils.isEmpty(playerName);
            if (hasPlayer && mAnimatedAtStartup) {
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mNameView.getWindowToken(), 0);
                if (player != null) {
                    PlayerActivity.startActivity(AlleyooopActivity.this, player);
                } else {
                    PlayerActivity.startActivity(AlleyooopActivity.this, playerName);
                }
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (isDirectionLeft(mPlayView)) {
                    imm.hideSoftInputFromWindow(mNameView.getWindowToken(), 0);
                } else {
                    imm.showSoftInput(mNameView, InputMethodManager.SHOW_FORCED);
                }
            }
            mAnimatedAtStartup = true;
            mPlayView.setTag(hasPlayer ? DIRECTION_RIGHT : isDirectionLeft(mPlayView) ? DIRECTION_RIGHT : DIRECTION_LEFT);
            mNameView.setText("");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // umeng
        MobclickAgent.openActivityDurationTrack(false);  // do not use activity duration track
        MobclickAgent.updateOnlineConfig(this);

        mShotPagerAdapter = new ShotPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.activity_alleyooop_pager);
        viewPager.setAdapter(mShotPagerAdapter);
        SlidingLinearTabPageIndicator indicator = (SlidingLinearTabPageIndicator) findViewById(R.id.activity_alleyooop_indicator);
        indicator.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                MobclickAgent.onEvent(AlleyooopActivity.this, ShotType.getUmengEvent(position));
            }
        });
        indicator.setViewPager(viewPager);
        indicator.setOnTabReselectedListener(this);
        mContainerView = (LinearLayout) findViewById(R.id.activity_alleyooop_container);
        mNameView = (EditText) findViewById(R.id.activity_alleyooop_name);
        mNameView.setVisibility(!TextUtils.isEmpty(PlayerUtil.getPlayerName(this)) ? View.INVISIBLE : View.VISIBLE);
        mPlayView = (ImageView) findViewById(R.id.activity_alleyooop_play);
        mPlayView.setTag(DIRECTION_LEFT);
        mPlayView.setOnClickListener(sPlayClickListener);
        mPlayView.setOnLongClickListener(new View.OnLongClickListener() { // long click to clear saved player information
            @Override
            public boolean onLongClick(View view) {
                if (!isDirectionLeft(view)) {
                    MobclickAgent.onEvent(AlleyooopActivity.this, Config.UmengEvents.MINE, "Clear Player Information");
                    PlayerUtil.clear(getApplicationContext());
                    Toast.makeText(getApplicationContext(), "Cleared Player Information", Toast.LENGTH_SHORT).show();
                    mNameView.setVisibility(View.VISIBLE);
                    view.performClick();
                }
                return true;
            }
        });
    }

    private boolean isDirectionLeft(View view) {
        return DIRECTION_LEFT == (Integer) view.getTag();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!mAnimatedAtStartup) {
            mPlayView.performClick();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_alleyooop;
    }

    @Override
    protected boolean isDisplayUpIndicator() {
        return false;
    }

    @Override
    protected boolean isDisplayIcon() {
        return true;
    }

    @Override
    protected String getScreenName() {
        return "AlleyooopActivity";
    }

    //region LinearTabPageIndicator.OnTabReselectedListener

    @Override
    public void onTabReselected(int position) {
        Fragment fragment = mShotPagerAdapter.getCurrentPrimaryItem();
        if (fragment != null && fragment instanceof BackToTop) {
            ((BackToTop) fragment).backToTop();
        }
    }

    //endregion

    //region ShotType

    public static enum ShotType {

        POPULAR("Popular"), EVERYONE("Everyone"), DEBUTS("Debuts");

        final String mName;

        ShotType(String name) {
            mName = name;
        }

        public String getName() {
            return mName;
        }

        static String getUmengEvent(int position) {
            switch (ShotType.values()[position]) {
                case POPULAR:
                    return Config.UmengEvents.SHOTS_POPULAR;
                case EVERYONE:
                    return Config.UmengEvents.SHOTS_EVERYONE;
                default:
                    return Config.UmengEvents.SHOTS_DEBUTS;
            }
        }

    }

    //endregion

    //region ShotPagerAdapter

    static final class ShotPagerAdapter extends LazyableFragmentStatePagerAdapter {

        ShotPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ShotFragment.newInstance(String.format("/shots/%s", ShotType.values()[position].getName().toLowerCase()), true);
        }

        @Override
        public int getCount() {
            return ShotType.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return ShotType.values()[position].getName();
        }

    }

    //endregion

}
