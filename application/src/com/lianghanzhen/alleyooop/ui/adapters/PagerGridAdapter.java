package com.lianghanzhen.alleyooop.ui.adapters;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.widget.AbsListView;
import android.widget.ListView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.lianghanzhen.alleyooop.models.PagerResult;
import com.lianghanzhen.alleyooop.models.requests.GsonRequest;
import com.lianghanzhen.alleyooop.models.requests.RequestManager;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.lianghanzhen.alleyooop.utils.UiUtil;


public abstract class PagerGridAdapter<T extends Parcelable, V extends PagerGridAdapter.BaseViewHolder, P extends PagerResult> extends GridAdapter<T, V>
        implements Response.Listener<P>, Response.ErrorListener, AbsListView.OnScrollListener {

    //region keys of save states

    private static final String KEY_PAGER_ADAPTER_CURRENT_PAGE = "KEY_PAGER_ADAPTER_CURRENT_PAGE";
    private static final String KEY_PAGER_ADAPTER_TOTAL_PAGES = "KEY_PAGER_ADAPTER_TOTAL_PAGES";
    private static final String KEY_PAGER_ADAPTER_COUNT_OF_PER_PAGE = "KEY_PAGER_ADAPTER_COUNT_OF_PER_PAGE";
    private static final String KEY_PAGER_ADAPTER_LOADING_STATE = "KEY_PAGER_ADAPTER_LOADING_STATE";

    //endregion

    // region pager request default values

    private static final int INVALID_PAGE = 0;
    private static final int DEFAULT_PER_PAGE = 15;
    private static final int MAX_PER_PAGE = 30;

    //endregion

    private LoadingState mState;
    private final ListView mListView;

    //region pager request info

    private final String mSuffixUrl;
    private int mCurrentPage;
    private int mTotalPages;
    private int mCountOfPerPage;

    //endregion

    private OnPageLoadingStateChangedListener mOnPageLoadingStateChangedListener;

    protected PagerGridAdapter(FragmentActivity activity, String suffixUrl, ListView listView) {
        super(activity);
        mSuffixUrl = suffixUrl;
        mCountOfPerPage = UiUtil.getCountOfPerPage(activity);
        mListView = listView;
        mListView.setOnScrollListener(this);
    }

    //region save and restore states

    @Override
    public void saveStates(Bundle outState) {
        super.saveStates(outState);
        outState.putInt(KEY_PAGER_ADAPTER_CURRENT_PAGE, mCurrentPage);
        outState.putInt(KEY_PAGER_ADAPTER_TOTAL_PAGES, mTotalPages);
        outState.putInt(KEY_PAGER_ADAPTER_COUNT_OF_PER_PAGE, mCountOfPerPage);
        outState.putSerializable(KEY_PAGER_ADAPTER_LOADING_STATE, mState);
    }

    @Override
    public void restoreStates(Bundle savedInstanceStates) {
        super.restoreStates(savedInstanceStates);
        mCurrentPage = BundleUtil.getInt(savedInstanceStates, KEY_PAGER_ADAPTER_CURRENT_PAGE, INVALID_PAGE);
        mTotalPages = BundleUtil.getInt(savedInstanceStates, KEY_PAGER_ADAPTER_TOTAL_PAGES, INVALID_PAGE);
        mCountOfPerPage = BundleUtil.getInt(savedInstanceStates, KEY_PAGER_ADAPTER_COUNT_OF_PER_PAGE, DEFAULT_PER_PAGE);
        setState(BundleUtil.getSerializable(savedInstanceStates, KEY_PAGER_ADAPTER_LOADING_STATE, LoadingState.PENDING));
    }

    //endregion

    //region Response.Listener<T>

    @Override
    public void onResponse(P result) {
        if (result.mMessage == null) {
            mCurrentPage = result.mCurrentPage;
            mTotalPages = result.mTotalPages;
            addResults(result.getResults());
        }
        setState(mCurrentPage >= mTotalPages ? LoadingState.END : LoadingState.PENDING);
    }

    //endregion

    //region Response.ErrorListener

    @Override
    public void onErrorResponse(VolleyError error) {
        setState(LoadingState.FAILURE);
    }

    //endregion

    //region AbsListView.OnScrollListener

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {}

    @Override
    public final void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mState != LoadingState.PENDING) {
            return;
        }
        if (firstVisibleItem + visibleItemCount >= totalItemCount
                && totalItemCount != 0
                && totalItemCount != mListView.getHeaderViewsCount() + mListView.getFooterViewsCount()
                && getCount() > 0
                && mCurrentPage < mTotalPages) {
            loadNextPage();
        }
    }

    //endregion

    //region load page

    public final void setCountOfPerPage(int countOfPerPage) {
        mCountOfPerPage = countOfPerPage <= 0 ? DEFAULT_PER_PAGE : countOfPerPage > MAX_PER_PAGE ? MAX_PER_PAGE : countOfPerPage;
    }

    public final void loadNextPage() {
        loadPage(mCurrentPage + 1);
    }

    public void reload() {
        mCurrentPage = INVALID_PAGE;
        mTotalPages = INVALID_PAGE;
        mPagerResults.clear();
        notifyDataSetChanged();
        loadNextPage();
    }

    protected void loadPage(int page) {
        setState(LoadingState.LOADING);
        if (mTotalPages > INVALID_PAGE && page > mTotalPages) {
            page = mTotalPages;
        }
        RequestManager.addRequest(newPagerRequest(String.format("%s?page=%d&per_page=%d", mSuffixUrl, page, mCountOfPerPage), this, this), mActivity);
    }

    protected abstract GsonRequest<P> newPagerRequest(String suffixUrl, Response.ErrorListener errorListener, Response.Listener<P> listener);

    //endregion

    //region LoadingState

    private void setState(LoadingState state) {
        boolean isStateChanged = mState != state;
        mState = state;
        if (isStateChanged && mOnPageLoadingStateChangedListener != null) {
            mOnPageLoadingStateChangedListener.onPageLoadingStateChanged(state);
        }
    }

    public static enum LoadingState {

        PENDING, LOADING, FAILURE, END

    }

    //endregion

    //region OnPageLoadingStateChangedListener

    public void setOnPageLoadingStateChangedListener(OnPageLoadingStateChangedListener onPageLoadingStateChangedListener) {
        mOnPageLoadingStateChangedListener = onPageLoadingStateChangedListener;
    }

    public interface OnPageLoadingStateChangedListener {

        void onPageLoadingStateChanged(LoadingState state);

    }

    //endregion

}
