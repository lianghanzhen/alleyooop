package com.lianghanzhen.alleyooop.ui.adapters;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.utils.UiUtil;

import java.util.ArrayList;
import java.util.List;


public abstract class GridAdapter<T extends Parcelable, V extends GridAdapter.BaseViewHolder> extends BaseAdapter {

    //region keys of save states

    private static final String KEY_GRID_ADAPTER_PAGER_RESULTS = "KEY_GRID_ADAPTER_PAGER_RESULTS";

    //endregion

    private static final String TAG_ITEM_VIEW = "TAG_ITEM_VIEW";

    protected final FragmentActivity mActivity;
    protected final LayoutInflater mLayoutInflater;
    protected final List<T> mPagerResults = new ArrayList<T>();
    private int mColumnCount;
    private final LinearLayout.LayoutParams sFirstItemParams;
    private final LinearLayout.LayoutParams sOtherItemsParams;

    private OnPageItemClickListener mOnPageItemClickListener;

    protected GridAdapter(FragmentActivity activity) {
        mActivity = activity;
        mLayoutInflater = LayoutInflater.from(activity);
        mColumnCount = UiUtil.getColumnCount(activity);
        sFirstItemParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        sOtherItemsParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        sOtherItemsParams.leftMargin = activity.getResources().getDimensionPixelSize(R.dimen.list_divider_height);
    }

    //region BaseAdapter

    @Override
    public final int getCount() {
        return (mPagerResults.size() / mColumnCount) + (hasRemain() ? 1 : 0);
    }

    protected final boolean hasRemain() {
        return mPagerResults.size() % mColumnCount != 0;
    }

    @Override
    public final List<T> getItem(int position) {
        List<T> items = new ArrayList<T>(mColumnCount);
        int size = mPagerResults.size();
        for (int i = 0; i < mColumnCount; i++) {
            int index = position * mColumnCount + i;
            items.add(index < size ? mPagerResults.get(index) : null);
        }
        return items;
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        List<T> items = getItem(position);
        List<V> viewHolders;
        LinearLayout layout;
        if (position == getCount() - 1 && hasRemain()) {
            layout = new LinearLayout(mActivity);
            viewHolders = new ArrayList<V>(addItemViews(layout, items, parent));
        } else if (convertView == null || !TAG_ITEM_VIEW.equals(convertView.getTag(R.id.tag_pager_adapter_item_view))) {
            layout = new LinearLayout(mActivity);
            viewHolders = new ArrayList<V>(addItemViews(layout, items, parent));
            layout.setTag(viewHolders);
            layout.setTag(R.id.tag_pager_adapter_item_view, TAG_ITEM_VIEW);
        } else {
            layout = (LinearLayout) convertView;
            viewHolders = (List<V>) layout.getTag();
        }

        for (int i = 0; i < mColumnCount; i++) {
            final T item = items.get(i);
            V viewHolder = viewHolders.get(i);
            if (item != null && viewHolder != null) {
                viewHolder.mItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnPageItemClickListener != null) {
                            mOnPageItemClickListener.onPageItemClick(item);
                        }
                    }
                });
                restoreViewHolder(item, viewHolder);
            }
        }

        return layout;
    }

    private List<V> addItemViews(LinearLayout layout, List<T> items, ViewGroup parent) {
        List<V> viewHolders = new ArrayList<V>(mColumnCount);
        for (int i = 0; i < mColumnCount; i++) {
            final T item = items.get(i);
            View itemView = item != null ? getItemView(item, null, parent) : new View(mActivity);
            V viewHolder = item != null ? newViewHolder(itemView) : null;
            if (viewHolder != null) {
                viewHolder.mItemView = itemView;
            }
            viewHolders.add(viewHolder);
            layout.addView(itemView, i == 0 ? sFirstItemParams : sOtherItemsParams);
        }
        return viewHolders;
    }

    protected abstract View getItemView(T item, V viewHolder, ViewGroup parent);

    protected abstract V newViewHolder(View itemView);

    protected abstract void restoreViewHolder(T item, V viewHolder);

    //endregion

    public void setColumnCount(int columnCount) {
        mColumnCount = columnCount;
    }

    //region save and restore states

    public void saveStates(Bundle outState) {
        outState.putParcelableArrayList(KEY_GRID_ADAPTER_PAGER_RESULTS, new ArrayList<Parcelable>(mPagerResults));
    }

    public void restoreStates(Bundle savedInstanceStates) {
        List<T> savedResults = savedInstanceStates.getParcelableArrayList(KEY_GRID_ADAPTER_PAGER_RESULTS);
        for (T result : savedResults) {
            mPagerResults.add(result);
        }
    }

    //endregion

    protected void addResults(List<T> results) {
        if (results != null && results.size() > 0) {
            mPagerResults.addAll(results);
            notifyDataSetChanged();
        }
    }


    //region BaseViewHolder

    /**
     * subclass's ViewHolder pattern must extends from BaseViewHolder to perform correct Item Click action
     */
    public static abstract class BaseViewHolder {
        View mItemView;
    }

    //endregion

    //region OnPageItemClickListener

    public void setOnPageItemClickListener(OnPageItemClickListener onPageItemClickListener) {
        mOnPageItemClickListener = onPageItemClickListener;
    }

    public interface OnPageItemClickListener<T> {
        void onPageItemClick(T item);
    }

    //endregion

}

