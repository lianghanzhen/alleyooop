package com.lianghanzhen.alleyooop.ui.fragments;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.Response;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Player;
import com.lianghanzhen.alleyooop.models.requests.GsonRequest;
import com.lianghanzhen.alleyooop.ui.PlayerActivity;
import com.lianghanzhen.alleyooop.ui.adapters.GridAdapter;
import com.lianghanzhen.alleyooop.ui.adapters.LazyableFragmentStatePagerAdapter;
import com.lianghanzhen.alleyooop.ui.adapters.PagerGridAdapter;
import com.lianghanzhen.alleyooop.utils.ImageLoader;
import com.lianghanzhen.alleyooop.utils.UiUtil;
import com.umeng.analytics.MobclickAgent;


public class PlayerFragment extends PagerFragment<PlayerFragment.PlayerAdapter> implements LazyableFragmentStatePagerAdapter.Lazyable {

    private static final String SCREEN_NAME = "PlayerFragment";

    public static PlayerFragment newInstance(String suffixUrl) {
        PlayerFragment fragment = new PlayerFragment();
        fragment.setArguments(newArguments(suffixUrl));
        return fragment;
    }

    private ImageLoader mImageLoader;

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(SCREEN_NAME);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(SCREEN_NAME);
    }

    @Override
    protected int getDefaultColumnCount() {
        return UiUtil.getPlayerColumnCount(getActivity());
    }

    @Override
    protected PlayerAdapter newPagerAdapter(FragmentActivity activity, String suffixUrl, ListView listView) {
        if (activity instanceof ImageLoader.ImageLoaderProvider) {
            mImageLoader = ((ImageLoader.ImageLoaderProvider) activity).getImageLoaderInstance();
        }
        return new PlayerAdapter(activity, suffixUrl, listView, mImageLoader);
    }

    @Override
    protected int getEmptyText() {
        return R.string.empty_no_players;
    }

    static final class PlayerAdapter extends PagerGridAdapter<Player, PlayerAdapter.ViewHolder, Player.Players> {

        private final ImageLoader mImageLoader;
        private final View.OnClickListener mAvatarClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayerActivity.startActivity(mActivity, (Player) view.getTag(R.id.tag_player));
                MobclickAgent.onEvent(mActivity, Config.UmengEvents.PLAYERS_DETAIL);
            }
        };

        PlayerAdapter(FragmentActivity activity, String suffixUrl, ListView listView, ImageLoader imageLoader) {
            super(activity, suffixUrl, listView);
            mImageLoader = imageLoader;
        }

        @Override
        protected GsonRequest<Player.Players> newPagerRequest(String suffixUrl, Response.ErrorListener errorListener, Response.Listener<Player.Players> listener) {
            return new GsonRequest<Player.Players>(String.format("%s%s", Config.sDribbbleApiUrl, suffixUrl), Player.Players.class, errorListener, listener);
        }

        @Override
        protected View getItemView(Player item, ViewHolder viewHolder, ViewGroup parent) {
            return mLayoutInflater.inflate(R.layout.list_item_player, parent, false);
        }

        @Override
        protected ViewHolder newViewHolder(View itemView) {
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mAvatarView = (ImageView) itemView.findViewById(R.id.list_item_player_avatar);
            viewHolder.mNameView = (TextView) itemView.findViewById(R.id.list_item_player_name);
            return viewHolder;
        }

        @Override
        protected void restoreViewHolder(Player item, ViewHolder viewHolder) {
            viewHolder.mAvatarView.setTag(R.id.tag_player, item);
            viewHolder.mAvatarView.setOnClickListener(mAvatarClickListener);
            mImageLoader.get(item.mAvatarUrl, viewHolder.mAvatarView, Config.INDEX_AVATAR_PLACEHOLDER);
            viewHolder.mNameView.setText(item.mName);
        }

        //region ViewHolder pattern

        static final class ViewHolder extends GridAdapter.BaseViewHolder {

            ImageView mAvatarView;
            TextView mNameView;

        }

        //endregion

    }

}
