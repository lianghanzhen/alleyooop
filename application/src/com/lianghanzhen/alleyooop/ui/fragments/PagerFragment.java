package com.lianghanzhen.alleyooop.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.RefreshCallback;
import com.lianghanzhen.alleyooop.ui.BackToTop;
import com.lianghanzhen.alleyooop.ui.adapters.PagerGridAdapter;
import com.lianghanzhen.alleyooop.ui.widgets.CustomListLayout;
import com.lianghanzhen.alleyooop.ui.widgets.ListFooterView;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.lianghanzhen.alleyooop.utils.UiUtil;


public abstract class PagerFragment<P extends PagerGridAdapter> extends Fragment implements RefreshCallback, BackToTop, PagerGridAdapter.OnPageLoadingStateChangedListener {

    private static final String KEY_SHOT_SUFFIX_URL = "KEY_SHOT_SUFFIX_URL";
    private static final String KEY_SHOT_COLUMN_COUNT = "KEY_SHOT_COLUMN_COUNT";

    private String mSuffixUrl;
    private CustomListLayout mListLayout;
    private P mPagerAdapter;
    private ListFooterView mFooterView;
    private View mHeaderView;

    private boolean mHasHeaders = true;
    private int mColumnCount;

    //region new arguments

    protected static Bundle newArguments(String suffixUrl) {
        return newArguments(suffixUrl, 0);
    }

    protected static Bundle newArguments(String suffixUrl, int columnCount) {
        Bundle arguments = new Bundle();
        arguments.putString(KEY_SHOT_SUFFIX_URL, suffixUrl);
        if (columnCount > 0) {
            arguments.putInt(KEY_SHOT_COLUMN_COUNT, columnCount);
        }
        return arguments;
    }

    //endregion

    //region Fragment Lifecycle

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSuffixUrl = BundleUtil.getString(getArguments(), KEY_SHOT_SUFFIX_URL, "");
        mColumnCount = BundleUtil.getInt(getArguments(), KEY_SHOT_COLUMN_COUNT, getDefaultColumnCount());
    }

    protected int getDefaultColumnCount() {
        return UiUtil.getColumnCount(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (mListLayout = new CustomListLayout(inflater.getContext()).setRefreshCallback(this));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // add ListView header and footer
        mHeaderView = new View(getActivity());
        if (mHasHeaders) {
            mListLayout.getListView().addHeaderView(mHeaderView);
        }
        mFooterView = new ListFooterView(getActivity());
        mListLayout.getListView().addFooterView(mFooterView);
        mPagerAdapter = newPagerAdapter(getActivity(), mSuffixUrl, mListLayout.getListView());
        mPagerAdapter.setColumnCount(mColumnCount);
        mPagerAdapter.setOnPageLoadingStateChangedListener(this);
        if (savedInstanceState != null) {
            mPagerAdapter.restoreStates(savedInstanceState);
        }
        mListLayout.getListView().setAdapter(mPagerAdapter);
    }

    protected abstract P newPagerAdapter(FragmentActivity activity, String suffixUrl, ListView listView);

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            mPagerAdapter.loadNextPage();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPagerAdapter != null) {
            mPagerAdapter.saveStates(outState);
        }
    }

    //endregion

    //region RefreshCallback

    @Override
    public void onRefresh() {
        if (mPagerAdapter != null) {
            mPagerAdapter.reload();
        }
    }

    //endregion

    public PagerFragment<P> hasHeaders(boolean hasHeaders) {
        mHasHeaders = hasHeaders;
        if (mListLayout != null && hasHeaders) {
            mListLayout.getListView().addHeaderView(mHeaderView);
        } else if (mListLayout != null) {
            mListLayout.getListView().removeHeaderView(mHeaderView);
        }
        return this;
    }

    //region BackToTop

    @Override
    public void backToTop() {
        UiUtil.smoothScrollListViewToTop(mListLayout.getListView());
    }

    //endregion

    //region PagerGridAdapter.OnPageLoadingStateChangedListener

    @Override
    public void onPageLoadingStateChanged(PagerGridAdapter.LoadingState state) {
        if (state == PagerGridAdapter.LoadingState.FAILURE) {
            if (mPagerAdapter.getCount() == 0) {
                mListLayout.showEmptyTextView().setEmptyText(R.string.loading_failure);
            } else {
                mFooterView.setText(R.string.loading_failure).showText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mFooterView.showProgressBar();
                        mPagerAdapter.loadNextPage();
                    }
                });
            }
        } else if (state == PagerGridAdapter.LoadingState.END) {
            if (mPagerAdapter.getCount() == 0) {
                mListLayout.setEmptyText(getEmptyText()).showEmptyTextView();
            } else {
                mListLayout.getListView().removeFooterView(mFooterView);
                mListLayout.getListView().addFooterView(new View(getActivity()));
            }
        }
    }

    //endregion

    protected int getEmptyText() {
        return R.string.empty_no_items;
    }

}
