package com.lianghanzhen.alleyooop.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Shot;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.lianghanzhen.alleyooop.utils.ImageLoader;
import com.umeng.analytics.MobclickAgent;

public class ShotDetailFragment extends Fragment {

    private static final String SCREEN_NAME = "ShotDetailFragment";

    private static final String KEY_SHOT_ITEM = "KEY_SHOT_ITEM";

    public static ShotDetailFragment newInstance(Shot shot) {
        ShotDetailFragment fragment = new ShotDetailFragment();
        Bundle arguments = new Bundle(1);
        arguments.putParcelable(KEY_SHOT_ITEM, shot);
        fragment.setArguments(arguments);
        return fragment;
    }

    private Shot mShot;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shot_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mShot = BundleUtil.getParcelable(savedInstanceState == null ? getArguments() : savedInstanceState, KEY_SHOT_ITEM, null);
        FragmentActivity activity = getActivity();
        if (activity instanceof ImageLoader.ImageLoaderProvider) {
            // todo change image url according to network
            ((ImageLoader.ImageLoaderProvider) activity).getImageLoaderInstance().get(mShot.mImageUrl,
                    (ImageView) view.findViewById(R.id.fragment_shot_detail_image), Config.INDEX_SHOT_PLACEHOLDER);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(SCREEN_NAME);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(SCREEN_NAME);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_SHOT_ITEM, mShot);
    }

}
