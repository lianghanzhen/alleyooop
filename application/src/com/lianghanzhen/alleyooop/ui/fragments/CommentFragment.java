package com.lianghanzhen.alleyooop.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.Response;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Comment;
import com.lianghanzhen.alleyooop.models.Player;
import com.lianghanzhen.alleyooop.models.Shot;
import com.lianghanzhen.alleyooop.models.requests.GsonRequest;
import com.lianghanzhen.alleyooop.ui.PlayerActivity;
import com.lianghanzhen.alleyooop.ui.adapters.GridAdapter;
import com.lianghanzhen.alleyooop.ui.adapters.PagerGridAdapter;
import com.lianghanzhen.alleyooop.ui.widgets.BezelImageView;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.lianghanzhen.alleyooop.utils.ImageLoader;
import com.lianghanzhen.alleyooop.utils.UiUtil;
import com.umeng.analytics.MobclickAgent;


public class CommentFragment extends PagerFragment<CommentFragment.CommentAdapter> {

    private static final String SCREEN_NAME = "CommentFragment";

    private static final String KEY_SHOT = "KEY_SHOT";

    public static CommentFragment newInstance(Shot shot) {
        CommentFragment fragment = new CommentFragment();
        Bundle arguments = newArguments(String.format("/shots/%d/comments", shot.mId), 1);
        arguments.putParcelable(KEY_SHOT, shot);
        fragment.setArguments(arguments);
        return fragment;
    }

    private ImageLoader mImageLoader;
    private Shot mShot;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShot = BundleUtil.getParcelable(savedInstanceState == null ? getArguments() : savedInstanceState, KEY_SHOT, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(SCREEN_NAME);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(SCREEN_NAME);
    }

    @Override
    protected CommentAdapter newPagerAdapter(FragmentActivity activity, String suffixUrl, ListView listView) {
        if (activity instanceof ImageLoader.ImageLoaderProvider) {
            mImageLoader = ((ImageLoader.ImageLoaderProvider) activity).getImageLoaderInstance();
        }
        return new CommentAdapter(activity, suffixUrl, listView, mImageLoader);
    }

    @Override
    protected int getEmptyText() {
        return R.string.empty_no_comments;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_SHOT, mShot);
    }

    static final class CommentAdapter extends PagerGridAdapter<Comment, CommentAdapter.ViewHolder, Comment.Comments> {

        private final ImageLoader mImageLoader;
        private final View.OnClickListener mAvatarClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayerActivity.startActivity(mActivity, (Player) view.getTag(R.id.tag_player));
                MobclickAgent.onEvent(mActivity, Config.UmengEvents.PLAYERS_DETAIL);
            }
        };

        CommentAdapter(FragmentActivity context, String suffixUrl, ListView listView, ImageLoader imageLoader) {
            super(context, suffixUrl, listView);
            mImageLoader = imageLoader;
        }

        @Override
        protected GsonRequest<Comment.Comments> newPagerRequest(String suffixUrl, Response.ErrorListener errorListener, Response.Listener<Comment.Comments> listener) {
            return new GsonRequest<Comment.Comments>(String.format("%s%s", Config.sDribbbleApiUrl, suffixUrl), Comment.Comments.class, errorListener, listener);
        }

        @Override
        protected View getItemView(Comment item, ViewHolder viewHolder, ViewGroup parent) {
            return mLayoutInflater.inflate(R.layout.list_item_comment, parent, false);
        }

        @Override
        protected ViewHolder newViewHolder(View itemView) {
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mAvatarView = (BezelImageView) itemView.findViewById(R.id.list_item_comment_avatar);
            viewHolder.mUsernameView = (TextView) itemView.findViewById(R.id.list_item_comment_username);
            viewHolder.mCommentBodyView = (TextView) itemView.findViewById(R.id.list_item_comment_body);
            return viewHolder;
        }

        @Override
        protected void restoreViewHolder(Comment item, ViewHolder viewHolder) {
            viewHolder.mAvatarView.setTag(R.id.tag_player, item.mPlayer);
            viewHolder.mAvatarView.setOnClickListener(mAvatarClickListener);
            mImageLoader.get(item.mPlayer.mAvatarUrl, viewHolder.mAvatarView, Config.INDEX_AVATAR_PLACEHOLDER);
            viewHolder.mUsernameView.setText(item.mPlayer.mName);
            UiUtil.setTextMaybeHtml(viewHolder.mCommentBodyView, item.mBody);
        }

        //region ViewHolder pattern

        static final class ViewHolder extends GridAdapter.BaseViewHolder {
            BezelImageView mAvatarView;
            TextView mUsernameView;
            TextView mCommentBodyView;
        }

        //endregion

    }

}
