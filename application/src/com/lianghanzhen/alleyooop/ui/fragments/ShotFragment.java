package com.lianghanzhen.alleyooop.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.Response;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Player;
import com.lianghanzhen.alleyooop.models.Shot;
import com.lianghanzhen.alleyooop.models.requests.GsonRequest;
import com.lianghanzhen.alleyooop.ui.PlayerActivity;
import com.lianghanzhen.alleyooop.ui.ShotDetailActivity;
import com.lianghanzhen.alleyooop.ui.adapters.GridAdapter;
import com.lianghanzhen.alleyooop.ui.adapters.LazyableFragmentStatePagerAdapter;
import com.lianghanzhen.alleyooop.ui.adapters.PagerGridAdapter;
import com.lianghanzhen.alleyooop.ui.widgets.BezelImageView;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.lianghanzhen.alleyooop.utils.ImageLoader;
import com.umeng.analytics.MobclickAgent;

public class ShotFragment extends PagerFragment<ShotFragment.ShotAdapter> implements LazyableFragmentStatePagerAdapter.Lazyable, GridAdapter.OnPageItemClickListener<Shot> {

    private static final String SCREEN_NAME = "ShotFragment";

    private static final String KEY_SHOW_HEADER = "KEY_SHOW_HEADER";

    public static ShotFragment newInstance(String suffixUrl, boolean showHeader) {
        ShotFragment fragment = new ShotFragment();
        Bundle arguments = newArguments(suffixUrl);
        arguments.putBoolean(KEY_SHOW_HEADER, showHeader);
        fragment.setArguments(arguments);
        return fragment;
    }

    public static ShotFragment newInstance(String suffixUrl, int columnCount, boolean showHeader) {
        ShotFragment fragment = new ShotFragment();
        Bundle arguments = newArguments(suffixUrl, columnCount);
        arguments.putBoolean(KEY_SHOW_HEADER, showHeader);
        fragment.setArguments(arguments);
        return fragment;
    }

    private ImageLoader mImageLoader;
    private boolean mShowHeader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShowHeader = BundleUtil.getBoolean(savedInstanceState == null ? getArguments() : savedInstanceState, KEY_SHOW_HEADER, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(SCREEN_NAME);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(SCREEN_NAME);
    }

    @Override
    protected ShotAdapter newPagerAdapter(FragmentActivity activity, String suffixUrl, ListView listView) {
        if (activity instanceof ImageLoader.ImageLoaderProvider) {
            mImageLoader = ((ImageLoader.ImageLoaderProvider) activity).getImageLoaderInstance();
        }
        ShotAdapter adapter = new ShotAdapter(activity, suffixUrl, listView, mImageLoader, mShowHeader);
        adapter.setOnPageItemClickListener(this);
        return adapter;
    }

    @Override
    protected int getEmptyText() {
        return R.string.empty_no_shots;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_SHOW_HEADER, mShowHeader);
    }

    //region PagerGridAdapter.OnPageItemClickListener

    @Override
    public void onPageItemClick(Shot item) {
        ShotDetailActivity.startActivity(getActivity(), item);
        MobclickAgent.onEvent(getActivity(), Config.UmengEvents.SHOTS_DETAIL);
    }

    //endregion

    //region ShotAdapter

    static final class ShotAdapter extends PagerGridAdapter<Shot, ShotAdapter.ViewHolder, Shot.Shots> {

        private final ImageLoader mImageLoader;
        private final View.OnClickListener mAvatarClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayerActivity.startActivity(mActivity, (Player) view.getTag(R.id.tag_player));
                MobclickAgent.onEvent(mActivity, Config.UmengEvents.PLAYERS_DETAIL);
            }
        };
        private final boolean mShowHeader;

        public ShotAdapter(FragmentActivity context, String suffixUrl, ListView listView, ImageLoader imageLoader, boolean showHeader) {
            super(context, suffixUrl, listView);
            mImageLoader = imageLoader;
            mShowHeader = showHeader;
        }

        @Override
        protected GsonRequest<Shot.Shots> newPagerRequest(String suffixUrl, Response.ErrorListener errorListener, Response.Listener<Shot.Shots> listener) {
            return new GsonRequest<Shot.Shots>(String.format("%s%s", Config.sDribbbleApiUrl, suffixUrl), Shot.Shots.class, errorListener, listener);
        }

        @Override
        protected View getItemView(Shot item, ViewHolder viewHolder, ViewGroup parent) {
            return mLayoutInflater.inflate(R.layout.list_item_shot, parent, false);
        }

        @Override
        protected ViewHolder newViewHolder(View itemView) {
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mHeaderView = itemView.findViewById(R.id.list_item_shot_header);
            viewHolder.mAvatarView = (BezelImageView) itemView.findViewById(R.id.list_item_shot_avatar);
            viewHolder.mUsernameView = (TextView) itemView.findViewById(R.id.list_item_shot_username);
            viewHolder.mImageView = (ImageView) itemView.findViewById(R.id.list_item_shot_image);
            viewHolder.mCreatedView = (TextView) itemView.findViewById(R.id.list_item_shot_time);
            viewHolder.mViewsCountView = (TextView) itemView.findViewById(R.id.list_item_shot_views);
            viewHolder.mCommentsCountView = (TextView) itemView.findViewById(R.id.list_item_shot_comments);
            viewHolder.mLikesCountView = (TextView) itemView.findViewById(R.id.list_item_shot_likes);
            return viewHolder;
        }

        @Override
        protected void restoreViewHolder(Shot shot, ViewHolder viewHolder) {
            viewHolder.mHeaderView.setVisibility(mShowHeader ? View.VISIBLE : View.GONE);
            if (mShowHeader) {
                viewHolder.mAvatarView.setTag(R.id.tag_player, shot.mPlayer);
                viewHolder.mAvatarView.setOnClickListener(mAvatarClickListener);
                mImageLoader.get(shot.mPlayer.mAvatarUrl, viewHolder.mAvatarView, Config.INDEX_AVATAR_PLACEHOLDER);
                viewHolder.mUsernameView.setText(shot.mPlayer.mName);
            }
            mImageLoader.get(shot.mImageUrl, viewHolder.mImageView, Config.INDEX_SHOT_PLACEHOLDER);  // todo change image url according to network
            viewHolder.mCreatedView.setText(Config.sDateFormat.format(shot.mCreatedAt));
            viewHolder.mViewsCountView.setText(String.valueOf(shot.mViewsCount));
            viewHolder.mCommentsCountView.setText(String.valueOf(shot.mCommentsCount));
            viewHolder.mLikesCountView.setText(String.valueOf(shot.mViewsCount));
        }

        static final class ViewHolder extends GridAdapter.BaseViewHolder {

            View mHeaderView;
            BezelImageView mAvatarView;
            TextView mUsernameView;
            ImageView mImageView;
            TextView mCreatedView;
            TextView mViewsCountView;
            TextView mCommentsCountView;
            TextView mLikesCountView;

        }

    }

    //endregion

}
