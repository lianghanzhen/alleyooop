package com.lianghanzhen.alleyooop.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.Shot;
import com.lianghanzhen.alleyooop.ui.adapters.LazyableFragmentStatePagerAdapter;
import com.lianghanzhen.alleyooop.ui.fragments.CommentFragment;
import com.lianghanzhen.alleyooop.ui.fragments.ShotDetailFragment;
import com.lianghanzhen.alleyooop.ui.fragments.ShotFragment;
import com.lianghanzhen.alleyooop.utils.BundleUtil;
import com.umeng.analytics.MobclickAgent;
import com.viewpagerindicator.SlidingLinearTabPageIndicator;

public class ShotDetailActivity extends BaseActionBarActivity {

    private static final String KEY_SHOT_ITEM = "KEY_SHOT_ITEM";

    public static void startActivity(Activity activity, Shot shot) {
        Intent intent = new Intent(activity, ShotDetailActivity.class);
        intent.putExtra(KEY_SHOT_ITEM, shot);
        activity.startActivity(intent);
    }

    private Shot mShot;
    private SlidingLinearTabPageIndicator mIndicator;
    private ViewPager mPager;
    private FrameLayout mCommentsContainer;
    private FrameLayout mReboundsContainer;

    private ShotDetailFragment mDetailFragment;
    private CommentFragment mCommentsFragment;
    private ShotFragment mReboundsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShot = (Shot) (savedInstanceState == null ? getIntent().getParcelableExtra(KEY_SHOT_ITEM)
                        : BundleUtil.getParcelable(savedInstanceState, KEY_SHOT_ITEM, null));
        loadAvatarImage(mShot.mPlayer.mAvatarUrl, mIconView);
        mTitleView.setText(mShot.mTitle);

        mIndicator = (SlidingLinearTabPageIndicator) findViewById(R.id.activity_shot_detail_indicator);
        mPager = (ViewPager) findViewById(R.id.activity_shot_detail_pager);
        mCommentsContainer = (FrameLayout) findViewById(R.id.activity_shot_detail_comments_container);
        mReboundsContainer = (FrameLayout) findViewById(R.id.activity_shot_detail_rebounds_container);
        if (mIndicator != null && mPager != null) {
            mPager.setAdapter(new ShotDetailPagerAdapter(getSupportFragmentManager(), mShot));
            mIndicator.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    MobclickAgent.onEvent(ShotDetailActivity.this, ShotDetailType.getUmengEvent(position));
                }
            });
            mIndicator.setViewPager(mPager);
        } else if (mCommentsContainer != null && mReboundsContainer != null) { // sw600dp-land
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(mCommentsContainer.getId(), CommentFragment.newInstance(mShot));
            fragmentTransaction.replace(mReboundsContainer.getId(), ShotFragment.newInstance(String.format("/shots/%d/rebounds", mShot.mId), 2, true).hasHeaders(false));
            fragmentTransaction.commit();
        }
        replaceWithDetailFragment();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_shot_detail;
    }

    @Override
    protected int getCustomActionBarViewResId() {
        return R.layout.action_bar_home_layout_with_player;
    }

    @Override
    protected boolean navigateUp() {
        boolean navigateUp = (mIndicator != null && mPager != null) || (mCommentsContainer != null && mReboundsContainer != null);
        if (!navigateUp) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_shot_detail_detail_container);
            if (fragment instanceof ShotDetailFragment) {
                navigateUp = true;
            } else {
                replaceWithDetailFragment();
            }
        }
        return navigateUp;
    }

    @Override
    protected String getScreenName() {
        return "ShotDetailActivity";
    }

    //region replace fragment

    private void replaceDetailContainer(Fragment replaceFragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_shot_detail_detail_container, replaceFragment).commit();
        supportInvalidateOptionsMenu();
    }

    private void replaceWithDetailFragment() {
        if (mDetailFragment == null) {
            mDetailFragment = ShotDetailFragment.newInstance(mShot);
        }
        replaceDetailContainer(mDetailFragment);
    }

    private void replaceWithCommentsFragment() {
        if (mCommentsFragment == null) {
            mCommentsFragment = CommentFragment.newInstance(mShot);
        }
        replaceDetailContainer(mCommentsFragment);
    }

    public void replaceWithReboundsFragment() {
        if (mReboundsFragment == null) {
            mReboundsFragment = ShotFragment.newInstance(String.format("/shots/%d/rebounds", mShot.mId), 2, true);
        }
        replaceDetailContainer(mReboundsFragment);
    }

    //endregion

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_SHOT_ITEM, mShot);
        if (mCommentsFragment != null) {
            mCommentsFragment.onSaveInstanceState(outState);
        }
        if (mReboundsFragment != null) {
            mReboundsFragment.onSaveInstanceState(outState);
        }
    }

    //region Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shot_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean hasMenu = mIndicator == null && mPager == null && mCommentsContainer == null && mReboundsContainer == null;
        if (hasMenu) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_shot_detail_detail_container);
            if (fragment instanceof ShotDetailFragment) {
                menu.findItem(R.id.shot_detail_details).setVisible(false);
            } else if (fragment instanceof ShotFragment) {
                menu.findItem(R.id.shot_detail_rebounds).setVisible(false);
            } else {
                menu.findItem(R.id.shot_detail_comments).setVisible(false);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.shot_detail_details:
                replaceWithDetailFragment();
                return true;
            case R.id.shot_detail_comments:
                replaceWithCommentsFragment();
                return true;
            case R.id.shot_detail_rebounds:
                replaceWithReboundsFragment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //endregion

    //region ShotDetailType

    static enum ShotDetailType {

        COMMENTS("Comments"), REBOUNDS("Rebounds");

        final String mName;

        ShotDetailType(String name) {
            mName = name;
        }

        static String getUmengEvent(int position) {
            return ShotDetailType.values()[position] == COMMENTS ? Config.UmengEvents.SHOTS_COMMENTS : Config.UmengEvents.SHOTS_REBOUNDS;
        }

    }

    //endregion

    //region ShotDetailPagerAdapter

    static final class ShotDetailPagerAdapter extends LazyableFragmentStatePagerAdapter {

        private final Shot mShot;

        ShotDetailPagerAdapter(FragmentManager fm, Shot shot) {
            super(fm);
            mShot = shot;
        }

        @Override
        public Fragment getItem(int position) {
            return ShotDetailType.values()[position] == ShotDetailType.COMMENTS ? CommentFragment.newInstance(mShot) : ShotFragment.newInstance(String.format("/shots/%d/rebounds", mShot.mId), true);
        }

        @Override
        public int getCount() {
            return ShotDetailType.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return ShotDetailType.values()[position].mName;
        }

    }

    //endregion

}
