package com.lianghanzhen.alleyooop.ui.widgets;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.lianghanzhen.alleyooop.R;

public class ListFooterView extends FrameLayout {

    private ProgressBar mProgressBar;
    private TextView mTextView;

    public ListFooterView(Context context) {
        super(context);
        inflateLayout(context);
    }

    public ListFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    public ListFooterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        inflate(context, R.layout.list_footer_view, this);
        mProgressBar = (ProgressBar) findViewById(R.id.list_footer_view_progress);
        mTextView = (TextView) findViewById(R.id.list_footer_view_text);
    }

    public ListFooterView showBoth() {
        mProgressBar.setVisibility(VISIBLE);
        mTextView.setVisibility(VISIBLE);
        return this;
    }

    public ListFooterView showProgressBar() {
        mProgressBar.setVisibility(VISIBLE);
        mTextView.setVisibility(GONE);
        return this;
    }

    public ListFooterView showText() {
        mProgressBar.setVisibility(GONE);
        mTextView.setVisibility(VISIBLE);
        return this;
    }

    public ListFooterView setText(CharSequence text) {
        mTextView.setText(text);
        return this;
    }

    public ListFooterView setText(int textResId) {
        mTextView.setText(textResId);
        return this;
    }

}
