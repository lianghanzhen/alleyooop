package com.lianghanzhen.alleyooop.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.RefreshCallback;


public class CustomListLayout extends FrameLayout {

    private ListView mListView;
    private View mEmptyView;
    private ProgressBar mProgressBar;
    private TextView mEmptyTextView;
    private RefreshCallback mRefreshCallback;
    private final OnClickListener mEmptyClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mEmptyTextView.getVisibility() == VISIBLE && mProgressBar.getVisibility() != VISIBLE && mRefreshCallback != null) {
                showProgress();
                mRefreshCallback.onRefresh();
            }
        }
    };

    public CustomListLayout(Context context) {
        super(context);
        initViews(context);
    }

    public CustomListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public CustomListLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(context);
    }

    private void initViews(Context context) {
        inflate(context, R.layout.custom_list_layout, this);
        mListView = (ListView) findViewById(android.R.id.list);
        mListView.setFadingEdgeLength(0);
        mEmptyView = findViewById(android.R.id.empty);
        mListView.setEmptyView(mEmptyView);
        mProgressBar = (ProgressBar) findViewById(R.id.list_layout_progress);
        mEmptyTextView = (TextView) findViewById(R.id.list_layout_text);
        setOnClickListener(null);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(mEmptyClickListener);
    }

    public ListView getListView() {
        return mListView;
    }

    public View getEmptyView() {
        return mEmptyView;
    }

    public CustomListLayout showProgress() {
        mEmptyView.setVisibility(VISIBLE);
        mProgressBar.setVisibility(VISIBLE);
        mEmptyTextView.setVisibility(GONE);
        return this;
    }

    public CustomListLayout showEmptyTextView() {
        mEmptyView.setVisibility(VISIBLE);
        mProgressBar.setVisibility(GONE);
        mEmptyTextView.setVisibility(VISIBLE);
        return this;
    }

    public CustomListLayout showBoth() {
        showEmptyTextView();
        mProgressBar.setVisibility(VISIBLE);
        mEmptyTextView.setVisibility(VISIBLE);
        return this;
    }

    public CustomListLayout setEmptyText(CharSequence text) {
        mEmptyTextView.setText(text);
        return this;
    }

    public CustomListLayout setEmptyText(int textResId) {
        mEmptyTextView.setText(textResId);
        return this;
    }

    public CustomListLayout setRefreshCallback(RefreshCallback refreshCallback) {
        mRefreshCallback = refreshCallback;
        return this;
    }

}
