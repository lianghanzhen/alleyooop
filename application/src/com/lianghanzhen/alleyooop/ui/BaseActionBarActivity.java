package com.lianghanzhen.alleyooop.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.lianghanzhen.alleyooop.Config;
import com.lianghanzhen.alleyooop.R;
import com.lianghanzhen.alleyooop.models.requests.RequestManager;
import com.lianghanzhen.alleyooop.utils.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;


public abstract class BaseActionBarActivity extends ActionBarActivity implements ImageLoader.ImageLoaderProvider {

    private final View.OnClickListener mNavigationUpClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (navigateUp()) {
                Intent parentIntent = NavUtils.getParentActivityIntent(BaseActionBarActivity.this);
                if (parentIntent != null) {
                    parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(parentIntent);
                } else {
                    finish();
                }
            }
        }
    };

    protected ImageView mIconView;
    protected TextView mTitleView;

    private static ArrayList<Drawable> sPlaceholderDrawables;
    private ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobclickAgent.setDebugMode(true);
        mImageLoader = new ImageLoader(this, getPlaceholderDrawables());
        setContentView(getContentView());
        configureActionBar();
    }

    private ArrayList<Drawable> getPlaceholderDrawables() {
        if (sPlaceholderDrawables == null) {
            sPlaceholderDrawables = new ArrayList<Drawable>(2);
            sPlaceholderDrawables.add(Config.INDEX_AVATAR_PLACEHOLDER, getResources().getDrawable(R.drawable.avatar_placeholder));
            sPlaceholderDrawables.add(Config.INDEX_SHOT_PLACEHOLDER, getResources().getDrawable(R.drawable.shot_placeholder));
        }
        return sPlaceholderDrawables;
    }

    protected abstract int getContentView();

    protected void configureActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        View customActionBarView = getLayoutInflater().inflate(getCustomActionBarViewResId(), null);
        mIconView = (ImageView) customActionBarView.findViewById(R.id.action_bar_home_layout_up_icon);
        mIconView.setVisibility(isDisplayIcon() ? View.VISIBLE : View.GONE);
        mTitleView = (TextView) customActionBarView.findViewById(R.id.action_bar_home_layout_up_title);
        mTitleView.setText(getTitle());
        mTitleView.setVisibility(isDisplayTitle() ? View.VISIBLE : View.GONE);
        customActionBarView.findViewById(R.id.action_bar_home_layout_up_indicator).setVisibility(isDisplayUpIndicator() ? View.VISIBLE : View.GONE);
        if (isDisplayUpIndicator()) {
            customActionBarView.setOnClickListener(mNavigationUpClickListener);
        }
        getSupportActionBar().setCustomView(customActionBarView);
    }

    protected int getCustomActionBarViewResId() {
        return R.layout.action_bar_home_layout;
    }

    protected boolean isDisplayUpIndicator() {
        return true;
    }

    protected boolean isDisplayIcon() {
        return true;
    }

    protected boolean isDisplayTitle() {
        return true;
    }

    protected boolean navigateUp() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(getScreenName())) {
            MobclickAgent.onPageStart(getScreenName());
        }
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!TextUtils.isEmpty(getScreenName())) {
            MobclickAgent.onPageEnd(getScreenName());
        }
        MobclickAgent.onPause(this);
    }

    /**
     * screen name for umeng track
     */
    protected String getScreenName() {
        return null;
    }

    @Override
    protected void onStop() {
        super.onStop();
        RequestManager.cancelRequests(this);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

    protected final void loadAvatarImage(String avatarUrl, ImageView avatarView) {
        mImageLoader.get(avatarUrl, avatarView, Config.INDEX_AVATAR_PLACEHOLDER);
    }

    protected final void loadShotImage(String shotUrl, ImageView shotView) {
        mImageLoader.get(shotUrl, shotView, Config.INDEX_SHOT_PLACEHOLDER);
    }

    //region ImageLoader.ImageLoaderProvider

    @Override
    public final ImageLoader getImageLoaderInstance() {
        return mImageLoader;
    }

    //endregion

}
